{-# LANGUAGE FlexibleInstances, OverloadedStrings, BangPatterns #-}

module TypeCheck where

import Types
import Debug.Trace

import qualified Data.List as L
import           Text.Printf (printf)
import           Control.Exception (throw)

--------------------------------------------------------------------------------

typeOfExpr :: Expr -> IO Type
typeOfExpr e = do
  let (!st, t) = infer initInferState preludeTypes e
  if (length (stSub st)) < 0 then throw (Error ("count Negative: " ++ show (stCnt st)))
  else return t

--------------------------------------------------------------------------------
-- Problem 1: Warm-up
--------------------------------------------------------------------------------

-- | Things that have free type variables
class HasTVars a where
  freeTVars :: a -> [TVar]

-- | Type variables of a type
instance HasTVars Type where
  freeTVars TInt = []
  freeTVars TBool = []
  freeTVars (t1 :=> t2) = L.nub $ freeTVars t1 ++ freeTVars t2
  freeTVars (TVar v) = [v]
  freeTVars (TList t) = freeTVars t

-- | Free type variables of a poly-type (remove forall-bound vars)
instance HasTVars Poly where
  freeTVars (Mono t)        = freeTVars t
  freeTVars (Forall tVar p) = L.delete tVar $ freeTVars p

-- | Free type variables of a type environment
instance HasTVars TypeEnv where
  freeTVars gamma   = concat [freeTVars s | (x, s) <- gamma]

-- | Lookup a variable in the type environment
lookupVarType :: Id -> TypeEnv -> Poly
lookupVarType x ((y, s) : gamma)
  | x == y    = s
  | otherwise = lookupVarType x gamma
lookupVarType x [] = throw (Error ("unbound variable: " ++ x))

-- | Extend the type environment with a new biding
extendTypeEnv :: Id -> Poly -> TypeEnv -> TypeEnv
extendTypeEnv x s gamma = (x,s) : gamma

-- | Lookup a type variable in a substitution;
--   if not present, return the variable unchanged
lookupTVar :: TVar -> Subst -> Type
lookupTVar a [] = TVar a
lookupTVar a (x:xs) =
    if a == var
    then t
    else lookupTVar a xs
  where
    (var, t) = x


-- | Remove a type variable from a substitution
removeTVar :: TVar -> Subst -> Subst
removeTVar a sub = removeTVarHelper a sub []
  where
    removeTVarHelper :: TVar -> Subst -> Subst -> Subst
    removeTVarHelper a [] destSub = destSub
    removeTVarHelper a (x:xs) destSub = if a == var
                                        then destSub ++ xs
                                        else removeTVarHelper a xs (destSub ++ [x])
      where
        (var, t) = x

-- | Things to which type substitutions can be apply
class Substitutable a where
  apply :: Subst -> a -> a

-- | Apply substitution to type
instance Substitutable Type where
  apply sub (TVar a)  = lookupTVar a sub
  apply sub (TList (TVar a))  = list (lookupTVar a sub)
  apply sub (t1 :=> t2) = (apply sub t1) :=> (apply sub t2)
  apply _ t = t


-- | Apply substitution to poly-type
instance Substitutable Poly where
  apply sub (Mono t)  = Mono (apply sub t)
  apply sub (Forall tVar p) = case p of
      (Mono t) -> Forall tVar (Mono $ apply sub' t)
      p' -> apply sub' p'
    where
      sub' = removeTVar tVar sub

-- | Apply substitution to (all poly-types in) another substitution
instance Substitutable Subst where
  apply sub to = zip keys $ map (apply sub) vals
    where
      (keys, vals) = unzip to

-- | Apply substitution to a type environment
instance Substitutable TypeEnv where
  apply sub gamma = zip keys $ map (apply sub) vals
    where
      (keys, vals) = unzip gamma

-- | Extend substitution with a new type assignment
extendSubst :: Subst -> TVar -> Type -> Subst
extendSubst sub a t = apply sub to
  where
    from = sub
    to = (a,t):(removeTVar a sub)

--------------------------------------------------------------------------------
-- Problem 2: Unification
--------------------------------------------------------------------------------

-- | State of the type inference algorithm
data InferState = InferState {
    stSub :: Subst -- ^ current substitution
  , stCnt :: Int   -- ^ number of fresh type variables generated so far
} deriving Show

-- | Initial state: empty substitution; 0 type variables
initInferState = InferState [] 0

-- | Fresh type variable number n
freshTV n = TVar $ "a" ++ show n

-- | Extend the current substitution of a state with a new type assignment
extendState :: InferState -> TVar -> Type -> InferState
extendState (InferState sub n) a t = InferState (extendSubst sub a t) n

-- | Unify a type variable with a type;
--   if successful return an updated state, otherwise throw an error
unifyTVar :: InferState -> TVar -> Type -> InferState
unifyTVar (InferState sub cnt) a t = case t of
  (TVar v) -> if a == v
              then InferState sub cnt -- case 1: unify "a" with "a"
              else inferStateNoA
  _ -> if containsA
    -- case 2: unify “a” with a type containing a free-var “a”
       then throw $ Error ("type error: cannot unify " ++ a ++ " and " ++ (typeString t) ++ " (occurs check)")
       else inferStateNoA
  where
    containsA = elem a $ freeTVars t
    -- case 3: unify type not containing free var "a"
    inferStateNoA = InferState (extendSubst sub a t) cnt

-- | Unify two types;
--   if successful return an updated state, otherwise throw an error
unify :: InferState -> Type -> Type -> InferState
unify st TInt TInt = st
unify st TBool TBool = st
unify st (TVar v) t = unifyTVar st v t
unify st t (TVar v) = unifyTVar st v t
unify st (TList t1) (TList t2) = unify st t1 t2
unify st (t1 :=> t2) (t1' :=> t2') = unify st1 pt2 pt2'
  where
    st1 = unify st t1 t1'
    pt2 = apply (stSub st1) t2
    pt2'= apply (stSub st1) t2'
unify st t1 t2 = throw $ Error $ "type error: cannot unify " ++ (typeString t1) ++ " and " ++ (typeString t2)

--------------------------------------------------------------------------------
-- Problem 3: Type Inference
--------------------------------------------------------------------------------

typeToList :: Type -> [Type]
typeToList TInt = [TInt]
typeToList TBool = [TBool]
typeToList (t1 :=> t2) = (typeToList t1) ++ (typeToList t2)
typeToList (TVar x) = [(TVar x)]
typeToList (TList t) = [(TList t)]

infer :: InferState -> TypeEnv -> Expr -> (InferState, Type)
infer st _   (EInt _)          = (st, TInt)
infer st _   (EBool _)         = (st, TBool)
infer st gamma (EVar x)        = (InferState (stSub st) ((stCnt st) + n),t)
  where
    (n,t) = instantiate (stCnt st) $ (lookupVarType x gamma)
infer st gamma (ELam x body)   = (st1, tX' :=> tBody)
  where
    tEnv' = extendTypeEnv x (Mono tX) gamma
    tX    = freshTV (stCnt st)
    stFresh = InferState (stSub st) ((stCnt st) + 1)
    (st1, tBody) = infer stFresh tEnv' body
    tX'          = apply (stSub st1) tX
infer st gamma (EApp e1 e2)    = (st3, fType)
  where
    (st1, tFun) = infer st gamma e1
    gamma' = apply (stSub st1) gamma
    (st2, tArg) = {-trace ("st1=" ++ (show st1) ++ " e1:" ++ (show e1) ++ " tFun:" ++ (typeString tFun)) $-} infer st1 gamma' e2
    freshVar = freshTV (stCnt st2)
    stFresh = {-trace ("st2=" ++ (show st2) ++ " e2:" ++ (show e2) ++ " tArg:" ++ (typeString tArg)) $-} InferState (stSub st2) ((stCnt st2) + 1)
    st3 = unify stFresh tFun (tArg :=> freshVar)
    fType = {-trace ( "freshVar:" ++ (show freshVar)) $-} apply (stSub st3) freshVar
infer st gamma (ELet x e1 e2)  = (st2, t2)
  where
    freshVar = freshTV (stCnt st)
    stFresh = InferState (stSub st) ((stCnt st) + 1)
    (TVar v) = freshVar
    gamma' = extendTypeEnv x (Forall v (Mono freshVar)) gamma

    (st1, t1) = {-trace ("gamma'=" ++ (show gamma')) $-} infer st gamma' e1
    gamma1 = apply (stSub st1) gamma
    s1 = generalize gamma1 t1
    gamma2 = extendTypeEnv x s1 gamma1

    -- do again because inside recursive statement might have pass ill-typed arg
    (st1', t1') = infer st gamma2 e1

    -- not sure if these 3 lines are needed
    gamma1' = apply (stSub st1') gamma2
    s1' = generalize gamma2 t1'
    gamma2' = extendTypeEnv x s1' gamma2

    (st2, t2) = {-trace ("gamma2=" ++ (show gamma2))$-} infer st1' gamma2' e2

infer st gamma (EBin op e1 e2) = infer st gamma asApp
  where
    asApp = EApp (EApp opVar e1) e2
    opVar = EVar (show op)
infer st gamma (EIf c e1 e2) = infer st gamma asApp
  where
    asApp = EApp (EApp (EApp ifVar c) e1) e2
    ifVar = EVar "if"
infer st gamma ENil = infer st gamma (EVar "[]")

-- | Generalize type variables inside a type
generalize :: TypeEnv -> Type -> Poly
generalize gamma t = generalize' forallList
  where
    listOfFreeTVars = freeTVars t

    listOfGammaVars [] = []
    listOfGammaVars (x:xs) = (getVarFromPair x) : listOfGammaVars xs

    getVarFromPair (_,Mono (TVar v)) = v
    getVarFromPair _ = "Error"

    forallList = (L.\\) listOfFreeTVars (listOfGammaVars gamma)

    generalize' [] = Mono $ t
    generalize' (x:xs) = Forall x (generalize' xs)

-- | Instantiate a polymorphic type into a mono-type with fresh type variables
instantiate :: Int -> Poly -> (Int, Type)
instantiate n s = instantiate' n s []
  where
    instantiate' n (Mono t) env = (n, apply env t)
    instantiate' n (Forall v p) env =
      instantiate' (n+1) p ((v,freshTV n):env)

-- | Types of built-in operators and functions
preludeTypes :: TypeEnv
preludeTypes =
  [ ("+",    Mono $ TInt :=> TInt :=> TInt)
  , ("-",    Mono $ TInt :=> TInt :=> TInt)
  , ("*",    Mono $ TInt :=> TInt :=> TInt)
  , ("/",    Mono $ TInt :=> TInt :=> TInt)
  , ("==",   Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("!=",   Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("<",    Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("<=",   Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("&&",   Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("||",   Forall "a" (Mono $ TVar "a" :=> TVar "a" :=> TBool))
  , ("if",   Forall "a" (Mono $ TBool :=> TVar "a" :=> TVar "a" :=> TVar "a"))
  -- lists: 
  , ("[]",   Forall "t" (Mono $ TList (TVar "t")))
  , (":",    Forall "a" (Mono $ TVar "a" :=> TList (TVar "a") :=> TList (TVar "a")))
  , ("head", Forall "a" (Mono $ TList (TVar "a") :=> TVar "a"))
  , ("tail", Forall "a" (Mono $ TList (TVar "a") :=> TList (TVar "a")))
  ]

