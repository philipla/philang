all:
	gcc interpreter.c lexer.c parser.c -o interpreter -g -Wall -Wextra -pedantic -std=c99

tests:
	gcc parser_tests.c lexer.c parser.c -o parser_tests -g -Wall -Wextra -pedantic -std=c99

.PHONY: clean
clean:
	rm interpreter parser_tests

