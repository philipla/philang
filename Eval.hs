{-# LANGUAGE OverloadedStrings #-}

module Eval where

import Control.Exception (throw, catch)
import Types
import TypeCheck

evaluate :: Expr -> IO String
evaluate e = (++) <$> expressionValue <*> expressionType
  where
    expressionValue = (execExpr e) >>= (\x -> return $ (show x) ++ " :: ")
    expressionType  = (typeOfExpr e) >>= (\x -> return (show x))

--------------------------------------------------------------------------------
execExpr :: Expr -> IO Value
--------------------------------------------------------------------------------
execExpr e = return (eval prelude e) `catch` exitError

--------------------------------------------------------------------------------
-- | `parse s` returns the Expr representation of the String s
--
-- >>> parse "True"
-- EBool True
--
-- >>> parse "False"
-- EBool False
--
-- >>> parse "123"
-- EInt 123
--
-- >>> parse "foo"
-- EVar "foo"
--
-- >>> parse "x + y"
-- EBin Plus (EVar "x") (EVar "y")
--
-- >>> parse "if x <= 4 then a || b else a && b"
-- EIf (EBin Le (EVar "x") (EInt 4)) (EBin Or (EVar "a") (EVar "b")) (EBin And (EVar "a") (EVar "b"))
--
-- >>> parse "if 4 <= z then 1 - z else 4 * z"
-- EIf (EBin Le (EInt 4) (EVar "z")) (EBin Minus (EInt 1) (EVar "z")) (EBin Mul (EInt 4) (EVar "z"))
--
-- >>> parse "let a = 6 * 2 in a /= 11"
-- ELet "a" (EBin Mul (EInt 6) (EInt 2)) (EBin Ne (EVar "a") (EInt 11))
--
-- >>> parseTokens "() (  )"
-- Right [LPAREN (AlexPn 0 1 1),RPAREN (AlexPn 1 1 2),LPAREN (AlexPn 3 1 4),RPAREN (AlexPn 6 1 7)]
--
-- >>> parse "f x"
-- EApp (EVar "f") (EVar "x")
--
-- >>> parse "(\\ x -> x + x) (3 * 3)"
-- EApp (ELam "x" (EBin Plus (EVar "x") (EVar "x"))) (EBin Mul (EInt 3) (EInt 3))
--
-- >>> parse "(((add3 (x)) y) z)"
-- EApp (EApp (EApp (EVar "add3") (EVar "x")) (EVar "y")) (EVar "z")
--
-- >>> parse <$> readFile "tests/input/t1.hs"
-- EBin Mul (EBin Plus (EInt 2) (EInt 3)) (EBin Plus (EInt 4) (EInt 5))
--
-- >>> parse <$> readFile "tests/input/t2.hs"
-- ELet "z" (EInt 3) (ELet "y" (EInt 2) (ELet "x" (EInt 1) (ELet "z1" (EInt 0) (EBin Minus (EBin Plus (EVar "x") (EVar "y")) (EBin Plus (EVar "z") (EVar "z1"))))))
--
-- >>> parse "1-2-3"
-- EBin Minus (EBin Minus (EInt 1) (EInt 2)) (EInt 3)
-- >>> parse "1+a&&b||c+d*e-f-g x"
-- EBin Or (EBin And (EBin Plus (EInt 1) (EVar "a")) (EVar "b")) (EBin Minus (EBin Minus (EBin Plus (EVar "c") (EBin Mul (EVar "d") (EVar "e"))) (EVar "f")) (EApp (EVar "g") (EVar "x")))
--
-- >>> parse "1:3:5:[]"
-- EBin Cons (EInt 1) (EBin Cons (EInt 3) (EBin Cons (EInt 5) ENil))
--
-- >>> parse "[1,3,5]"
-- EBin Cons (EInt 1) (EBin Cons (EInt 3) (EBin Cons (EInt 5) ENil))

exitError :: Error -> IO Value
exitError (Error msg) = return (VErr msg)

--------------------------------------------------------------------------------
-- | `eval env e` evaluates the Nano expression `e` in the environment `env`
--   (i.e. uses `env` for the values of the **free variables** in `e`),
--   and throws an `Error "unbound variable"` if the expression contains
--   a free variable that is **not bound** in `env`.
--
-- part (a)
--
-- >>> eval env0 (EBin Minus (EBin Plus "x" "y") (EBin Plus "z" "z1"))
-- 0
--
-- >>> eval env0 "p"
-- *** Exception: Error {errMsg = "unbound variable: p"}
--
-- part (b)
--
-- >>> eval []  (EBin Le (EInt 2) (EInt 3))
-- True
--
-- >>> eval []  (EBin Eq (EInt 2) (EInt 3))
-- False
--
-- >>> eval []  (EBin Eq (EInt 2) (EBool True))
-- False
--
-- >>> eval []  (EBin Lt (EInt 2) (EBool True))
-- *** Exception: Error {errMsg = "type error: binop"}
--
-- >>> let e1 = EIf (EBin Lt "z1" "x") (EBin Ne "y" "z") (EBool False)
-- >>> eval env0 e1
-- True
--
-- >>> let e2 = EIf (EBin Eq "z1" "x") (EBin Le "y" "z") (EBin Le "z" "y")
-- >>> eval env0 e2
-- False
--
-- part (c)
--
-- >>> let e1 = EBin Plus "x" "y"
-- >>> let e2 = ELet "x" (EInt 1) (ELet "y" (EInt 2) e1)
-- >>> eval [] e2
-- 3
--
-- part (d)
--
-- >>> eval [] (EApp (ELam "x" (EBin Plus "x" "x")) (EInt 3))
-- 6
--
-- >>> let e3 = ELet "h" (ELam "y" (EBin Plus "x" "y")) (EApp "f" "h")
-- >>> let e2 = ELet "x" (EInt 100) e3
-- >>> let e1 = ELet "f" (ELam "g" (ELet "x" (EInt 0) (EApp "g" (EInt 2)))) e2
-- >>> eval [] e1
-- 102
--
-- part (e)
-- |
-- >>> :{
-- eval [] (ELet "fac" (ELam "n" (EIf (EBin Eq "n" (EInt 0))
--                                  (EInt 1)
--                                  (EBin Mul "n" (EApp "fac" (EBin Minus "n" (EInt 1))))))
--             (EApp "fac" (EInt 10)))
-- :}
-- 3628800
--
-- part (f)
--
-- >>> let el = EBin Cons (EInt 1) (EBin Cons (EInt 2) ENil)
-- >>> execExpr el
-- (1 : (2 : []))
-- >>> execExpr (EApp "head" el)
-- 1
-- >>> execExpr (EApp "tail" el)
-- (2 : [])
--------------------------------------------------------------------------------
eval :: Env -> Expr -> Value
--------------------------------------------------------------------------------
eval _ (EInt x) = VInt x
eval _ (EBool x) = VBool x
eval e (EVar x) = lookupId x e
eval e (EBin op e1 e2) = evalOp op (eval e e1) (eval e e2)
eval e (EIf p e1 e2) =
  case (eval e p) of
    (VBool True)  -> eval e e1
    (VBool False) -> eval e e2
    _             -> throw $ Error ("type error: if then else")
eval e (ELet name e1 e2) = eval (pair:e) e2
  where
    pair = (name, eval e e1)
eval e (ELam name e1) = VClos e name e1
eval _ ENil = VNil
eval e (EApp e1 e2)   = evalAppHelper (eval e e1) v e
  where
    v = eval e e2
--eval _ _ = throw $ Error ("type error: eval")

evalAppHelper :: Value -> Value -> Env -> Value
evalAppHelper (VClos ep name ec) v e = eval ((name,v):(ep++e)) ec
evalAppHelper (VPrim f)          v _ = f v
evalAppHelper v                  _ _ = v

--------------------------------------------------------------------------------
evalOp :: Binop -> Value -> Value -> Value
--------------------------------------------------------------------------------
evalOp Plus (VInt x) (VInt y) = VInt $ (+) x y
evalOp Minus (VInt x) (VInt y) = VInt $ (-) x y
evalOp Mul (VInt x) (VInt y) = VInt $ (*) x y
evalOp Div (VInt x) (VInt y) = VInt $ div x y
evalOp Eq (VInt x) (VInt y) = VBool $ (==) x y
evalOp Eq VNil VNil = VBool True
evalOp Eq _ VNil = VBool False
evalOp Eq VNil _ = VBool False
evalOp Eq (VPair v1 v2) (VPair v1' v2') = VBool $ r1 && r2
  where
    (VBool r1) = evalOp Eq v1 v1'
    (VBool r2) = evalOp Eq v2 v2'
evalOp Ne (VInt x) (VInt y) = VBool $ (/=) x y
evalOp Ne VNil VNil = VBool False
evalOp Ne _ VNil = VBool True
evalOp Ne VNil _ = VBool True
evalOp Ne (VPair v1 v2) (VPair v1' v2') = VBool $ not (r1 && r2)
  where
    (VBool r1) = evalOp Eq v1 v1'
    (VBool r2) = evalOp Eq v2 v2'
evalOp Lt (VInt x) (VInt y) = VBool $ (<) x y
evalOp Le (VInt x) (VInt y) = VBool $ (<=) x y
evalOp Cons v xs = VPair v xs
evalOp And (VBool x) (VBool y) = VBool $ (&&) x y
evalOp Or (VBool x) (VBool y) = VBool $ (||) x y
evalOp _ _ _ = throw $ Error ("type error: binop")

--------------------------------------------------------------------------------
-- | `lookupId x env` returns the most recent
--   binding for the variable `x` (i.e. the first
--   from the left) in the list representing the
--   environment, and throws an `Error` otherwise.
--
-- >>> lookupId "z1" env0
-- 0
-- >>> lookupId "x" env0
-- 1
-- >>> lookupId "y" env0
-- 2
-- >>> lookupId "mickey" env0
-- *** Exception: Error {errMsg = "unbound variable: mickey"}
--------------------------------------------------------------------------------
lookupId :: Id -> Env -> Value
--------------------------------------------------------------------------------
lookupId s [] = throw $ Error ("unbound variable: " ++ s)
lookupId s (x:xs) =
  if s == (fst x)
  then snd x
  else lookupId s xs

head' :: Value -> Value
head' (VPair v _) = v
head' _ = throw $ Error ("prelude head error")

tail' :: Value -> Value
tail' (VPair _ xs) = xs
tail' _ = throw $ Error ("prelude tail error")

prelude :: Env
prelude =
  [ -- HINT: you may extend this "built-in" environment
    -- with some "operators" that you find useful...
    ("id", VClos [] "x" (EVar "x")),
    ("head", VPrim head'),
    ("tail", VPrim tail')
  ]

env0 :: Env
env0 =  [ ("z1", VInt 0)
        , ("x" , VInt 1)
        , ("y" , VInt 2)
        , ("z" , VInt 3)
        , ("z1", VInt 4)
        ]

--------------------------------------------------------------------------------
