#include <stdio.h>
#include <stdlib.h>
#include "lexer.h"
#include "parser.h"

char
stringEquals(char *string, char *match)
{
    char *at = match;
    for (unsigned int i = 0; i < stringLength(string); i++, at++)
    {
        if (*at == 0 || string[i] != *at)
        {
            return 0;
        }
    }

    return *at == 0;
}

#define TEST(t,e) \
    test = (t); \
    expected = (e); \
    parser((t), &output); \
    if (stringEquals(output, expected)) \
    { \
        printf("\x1B[32mPASS\x1B[37m test=%s actual=%s expected=%s\n", test, output, expected); \
    } \
    else \
    { \
        printf("\x1B[31mFAIL\x1B[37m test=%s actual=%s expected=%s\n", test, output, expected); \
    } \
    free(output); \


int main(void)
{
    char *output;
    char *test, *expected;

    TEST("True", "((EBool True))");
    TEST("123", "((EInt 123))");
    TEST("x", "((EVar \\\"x\\\"))");
    TEST("let x = 5 in x", "((ELet \\\"x\\\" ( EInt 5) (EVar \\\"x\\\")))");
    TEST("\\x -> 5", "((ELam \\\"x\\\" (EInt 5)))");
    TEST("if a then b else c", "((EIf ( EVar \\\"a\\\") ( EVar \\\"b\\\") ( EVar \\\"c\\\")))");
    TEST("x + 2", "EBin Plus (((EVar \\\"x\\\"))) ((( EInt 2)))");
    TEST("x <= 2", "((EBin Le (EVar \\\"x\\\") (EInt 2)))");
    TEST("x && 2", "((EBin And (EVar \\\"x\\\") (EInt 2)))");
    TEST("1 + ( 2 * ( 3 ) )", "EBin Plus (((EInt 1))) ((( (((EBin Mul (( EInt 2)) (( (((( EInt 3))))))))))))");
    TEST("f x", "((EApp (EVar \\\"f\\\") (EVar \\\"x\\\")))");
    TEST("e : f", "((EBin Cons (EVar \\\"e\\\") (EVar \\\"f\\\")))");
    TEST("e : f", "((EBin Cons (EVar \\\"e\\\") (EVar \\\"f\\\")))");
    TEST("(2 + 3) * (4 + 5)", "(EBin Mul ((((EBin Plus (((EInt 2))) ((( EInt 3))))))) (( ((EBin Plus (((EInt 4))) ((( EInt 5))))))))");
    TEST("let z = 3 in let y = 2 in let x = 1 in let z1 = 0 in (x + y) - (z + z1)", "((ELet \\\"z\\\" ( EInt 3) (ELet \\\"y\\\" ( EInt 2) (ELet \\\"x\\\" ( EInt 1) (ELet \\\"z1\\\" ( EInt 0) (EBin Minus (((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\")))))) (((EBin Plus (((EVar \\\"z\\\"))) ((( EVar \\\"z1\\\"))))))))))))");
    TEST("let z1 = 4 in let z = 3 in let y = 2 in let x = 1 in let z2 = 0 in if (((x+y)-(z+z2)) == 0) then y else x", "((ELet \\\"z1\\\" ( EInt 4) (ELet \\\"z\\\" ( EInt 3) (ELet \\\"y\\\" ( EInt 2) (ELet \\\"x\\\" ( EInt 1) (ELet \\\"z2\\\" ( EInt 0) (EIf ( ((((EBin Eq (((((EBin Minus (((EBin Plus (((EVar \\\"x\\\"))) (((EVar \\\"y\\\")))))) (((EBin Plus (((EVar \\\"z\\\"))) (((EVar \\\"z2\\\"))))))))))) (EInt 0)))))) ( EVar \\\"y\\\") ( EVar \\\"x\\\"))))))))");
    TEST("let z  = 3 in let y  = 2 in let x  = 1 in let z1 = 0 in if x == 0 then y else p", "((ELet \\\"z\\\" ( EInt 3) (ELet \\\"y\\\" ( EInt 2) (ELet \\\"x\\\" ( EInt 1) (ELet \\\"z1\\\" ( EInt 0) (EIf ( EBin Eq (EVar \\\"x\\\") (EInt 0)) ( EVar \\\"y\\\") ( EVar \\\"p\\\")))))))");
    TEST("let x  = 1 in let y  = 2 in let z  = (x + y) in let x1 = (x + z) in (x1 + y)", "((ELet \\\"x\\\" ( EInt 1) (ELet \\\"y\\\" ( EInt 2) (ELet \\\"z\\\" ( ((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\")))))) (ELet \\\"x1\\\" ( ((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"z\\\")))))) (((EBin Plus (((EVar \\\"x1\\\"))) ((( EVar \\\"y\\\")))))))))))");
    TEST("let f = \\g -> let x = 0 in g 2 in let x = 100 in let h = \\ y -> (x + y) in f h", "((ELet \\\"f\\\" ( ELam \\\"g\\\" (ELet \\\"x\\\" ( EInt 0) (EApp (EVar \\\"g\\\") (EInt 2)))) (ELet \\\"x\\\" ( EInt 100) (ELet \\\"h\\\" ( ELam \\\"y\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\"))))))) (EApp (EVar \\\"f\\\") (EVar \\\"h\\\"))))))");
    TEST("let add = \\x -> if x <= 0 then 0 else (x + (add (x-1))) in add 10", "((ELet \\\"add\\\" ( ELam \\\"x\\\" (EIf ( EBin Le (EVar \\\"x\\\") (EInt 0)) ( EInt 0) ( ((EBin Plus (((EVar \\\"x\\\"))) ((( ((((EApp (EVar \\\"add\\\") (((((EBin Minus (EVar \\\"x\\\") (EInt 1)))))))))))))))))) (EApp (EVar \\\"add\\\") (EInt 10))))");
    TEST("let fac = \\x -> if x <= 0 then 1 else (x * (fac (x - 1))) in fac 10", "((ELet \\\"fac\\\" ( ELam \\\"x\\\" (EIf ( EBin Le (EVar \\\"x\\\") (EInt 0)) ( EInt 1) ( (((EBin Mul ((EVar \\\"x\\\")) (( ((((EApp (EVar \\\"fac\\\") (((((EBin Minus (EVar \\\"x\\\") (EInt 1)))))))))))))))))) (EApp (EVar \\\"fac\\\") (EInt 10))))");
    TEST("let f = \\x -> \\y -> (x + y) in let g = f 10 in g 100", "((ELet \\\"f\\\" ( ELam \\\"x\\\" (ELam \\\"y\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\")))))))) (ELet \\\"g\\\" ( EApp (EVar \\\"f\\\") (EInt 10)) (EApp (EVar \\\"g\\\") (EInt 100)))))");
    TEST("let foldn f b n = let loop i c = if i <= n then (loop (i+1)) ((f i) c) else c in (loop 0) b in let add = (foldn (\\x -> \\y -> (x + y))) 0 in add 10", "((ELet \\\"foldn\\\" (ELam \\\"f\\\" (ELam \\\"b\\\" (ELam \\\"n\\\" (ELet \\\"loop\\\" (ELam \\\"i\\\" (ELam \\\"c\\\" (EIf ( EBin Le (EVar \\\"i\\\") (EVar \\\"n\\\")) ( EApp (((((EApp (EVar \\\"loop\\\") (((EBin Plus (((EVar \\\"i\\\"))) (((EInt 1))))))))))) (((((EApp (((((EApp (EVar \\\"f\\\") (EVar \\\"i\\\")))))) (EVar \\\"c\\\"))))))) ( EVar \\\"c\\\")))) (EApp (((((EApp (EVar \\\"loop\\\") (EInt 0)))))) (EVar \\\"b\\\")))))) (ELet \\\"add\\\" ( EApp (((((EApp (EVar \\\"foldn\\\") (((((ELam \\\"x\\\" (ELam \\\"y\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\"))))))))))))))))) (EInt 0)) (EApp (EVar \\\"add\\\") (EInt 10)))))");
    TEST("let foldn f b n = let loop i c = if i <= n then (loop (i+1)) ((f i) c) else c in (loop 0) b in let fac = (foldn (\\x -> \\y -> (if x == 0 then 1 else (x * y)))) 1 in fac 10", "((ELet \\\"foldn\\\" (ELam \\\"f\\\" (ELam \\\"b\\\" (ELam \\\"n\\\" (ELet \\\"loop\\\" (ELam \\\"i\\\" (ELam \\\"c\\\" (EIf ( EBin Le (EVar \\\"i\\\") (EVar \\\"n\\\")) ( EApp (((((EApp (EVar \\\"loop\\\") (((EBin Plus (((EVar \\\"i\\\"))) (((EInt 1))))))))))) (((((EApp (((((EApp (EVar \\\"f\\\") (EVar \\\"i\\\")))))) (EVar \\\"c\\\"))))))) ( EVar \\\"c\\\")))) (EApp (((((EApp (EVar \\\"loop\\\") (EInt 0)))))) (EVar \\\"b\\\")))))) (ELet \\\"fac\\\" ( EApp (((((EApp (EVar \\\"foldn\\\") (((((ELam \\\"x\\\" (ELam \\\"y\\\" (((((EIf ( EBin Eq (EVar \\\"x\\\") (EInt 0)) ( EInt 1) ( (((EBin Mul ((EVar \\\"x\\\")) (( EVar \\\"y\\\")))))))))))))))))))))) (EInt 1)) (EApp (EVar \\\"fac\\\") (EInt 10)))))");
    TEST("let f = \\x -> \\y -> \\a -> (a x * y) in let g = \\x -> (x + 1 * 3)           in f 7 8 g", "((ELet \\\"f\\\" ( ELam \\\"x\\\" (ELam \\\"y\\\" (ELam \\\"a\\\" ((((EBin Mul ((EApp (EVar \\\"a\\\") (EVar \\\"x\\\"))) (( EVar \\\"y\\\"))))))))) (ELet \\\"g\\\" ( ELam \\\"x\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((EBin Mul (( EInt 1)) (( EInt 3)))))))) (EApp (EApp (EApp (EVar \\\"f\\\") (EInt 7)) (EInt 8)) (EVar \\\"g\\\")))))");
    TEST("let l = [1, 2, 3, 4] in let m = [5, 6, 7, 8] in head l : tail m", "((ELet \\\"l\\\" ( EBin Cons (EInt 1) (EBin Cons (EInt 2 ) (EBin Cons (EInt 3 ) (EBin Cons (EInt 4 ) (ENil))))) (ELet \\\"m\\\" ( EBin Cons (EInt 5) (EBin Cons (EInt 6 ) (EBin Cons (EInt 7 ) (EBin Cons (EInt 8 ) (ENil))))) (EBin Cons (EApp (EVar \\\"head\\\") (EVar \\\"l\\\")) (EApp (EVar \\\"tail\\\") (EVar \\\"m\\\"))))))");
    TEST("let even x = let odd x = if x == 0 then False else even (x - 1) in if x == 0 then True else odd (x-1) in even 23", "((ELet \\\"even\\\" (ELam \\\"x\\\" (ELet \\\"odd\\\" (ELam \\\"x\\\" (EIf ( EBin Eq (EVar \\\"x\\\") (EInt 0)) ( EBool False) ( EApp (EVar \\\"even\\\") (((((EBin Minus (EVar \\\"x\\\") (EInt 1))))))))) (EIf ( EBin Eq (EVar \\\"x\\\") (EInt 0)) ( EBool True) ( EApp (EVar \\\"odd\\\") (((((EBin Minus (EVar \\\"x\\\") (EInt 1)))))))))) (EApp (EVar \\\"even\\\") (EInt 23))))");
    // TODO(phil): are we ever to fix the let/lambda arith paren bound \x -> x + 1? where we need to put the parens. see below test
    TEST("let map f xs = if xs == [] then [] else let h = head xs in let t = tail xs in f h : map f t in let incr x = (x + 1) in let l = [1, 2, 3, 4] in map incr l", "((ELet \\\"map\\\" (ELam \\\"f\\\" (ELam \\\"xs\\\" (EIf ( EBin Eq (EVar \\\"xs\\\") (ENil)) ( ENil) ( ELet \\\"h\\\" ( EApp (EVar \\\"head\\\") (EVar \\\"xs\\\")) (ELet \\\"t\\\" ( EApp (EVar \\\"tail\\\") (EVar \\\"xs\\\")) (EBin Cons (EApp (EVar \\\"f\\\") (EVar \\\"h\\\")) (EApp (EApp (EVar \\\"map\\\") (EVar \\\"f\\\")) (EVar \\\"t\\\")))))))) (ELet \\\"incr\\\" (ELam \\\"x\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((( EInt 1))))))) (ELet \\\"l\\\" ( EBin Cons (EInt 1) (EBin Cons (EInt 2 ) (EBin Cons (EInt 3 ) (EBin Cons (EInt 4 ) (ENil))))) (EApp (EApp (EVar \\\"map\\\") (EVar \\\"incr\\\")) (EVar \\\"l\\\"))))))");
    TEST("let foldr f b xs = if xs == [] then b else let h = head xs in let t = tail xs in f h (foldr f b t) in let add x y = (x + y) in let l = [1, 2, 3, 4] in foldr add 0 l", "((ELet \\\"foldr\\\" (ELam \\\"f\\\" (ELam \\\"b\\\" (ELam \\\"xs\\\" (EIf ( EBin Eq (EVar \\\"xs\\\") (ENil)) ( EVar \\\"b\\\") ( ELet \\\"h\\\" ( EApp (EVar \\\"head\\\") (EVar \\\"xs\\\")) (ELet \\\"t\\\" ( EApp (EVar \\\"tail\\\") (EVar \\\"xs\\\")) (EApp (EApp (EVar \\\"f\\\") (EVar \\\"h\\\")) (((((EApp (EApp (EApp (EVar \\\"foldr\\\") (EVar \\\"f\\\")) (EVar \\\"b\\\")) (EVar \\\"t\\\"))))))))))))) (ELet \\\"add\\\" (ELam \\\"x\\\" (ELam \\\"y\\\" (((EBin Plus (((EVar \\\"x\\\"))) ((( EVar \\\"y\\\")))))))) (ELet \\\"l\\\" ( EBin Cons (EInt 1) (EBin Cons (EInt 2 ) (EBin Cons (EInt 3 ) (EBin Cons (EInt 4 ) (ENil))))) (EApp (EApp (EApp (EVar \\\"foldr\\\") (EVar \\\"add\\\")) (EInt 0)) (EVar \\\"l\\\"))))))");

    

    







    return 0;
}
