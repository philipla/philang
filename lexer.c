#include "lexer.h"

char
isAlpha(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

char
isNumber(char c)
{
    return (c >= '0' && c <= '9');
}

char
isEndOfLine(char c) { return c == '\n' || c == '\r'; }

char
isTabOrLine(char c) { return c == '\t' || isEndOfLine(c); }

void
removeTabsLinesComments(Tokenizer *tokenizer)
{
    while (isTabOrLine(tokenizer->at[0]))
    {
        if (isTabOrLine(tokenizer->at[0]))
        {
            ++tokenizer->at;
        }
        else if (tokenizer->at[0] == '#')
        {
            while (tokenizer->at[0] && isEndOfLine(tokenizer->at[0]))
            {
                ++tokenizer->at;
            }
        }
        else
        {
            break;
        }
    }
}

char
tokenEquals(Token token, char *match)
{
    char *at = match;
    for (int index = 0; index < token.textLength; ++index, ++at)
    {
        if (*at == 0 || token.text[index] != *at)
        {
            return 0;
        }
    }

    return *at == 0;
}

char
requireToken(Tokenizer *tokenizer, Token_Type desiredType)
{
    Token token = getNonSpaceToken(tokenizer);
    return token.type == desiredType;
}

Token
getToken(Tokenizer *tokenizer)
{
    removeTabsLinesComments(tokenizer);

    Token token;
    token.textLength = 1;
    token.text = tokenizer->at;

    switch (tokenizer->at[0])
    {
        case '\0': { token.type = TOKEN_EOF; ++tokenizer->at; } break;
        case '\\': { token.type = TOKEN_LAM; ++tokenizer->at; } break;
        case ' ': { token.type = TOKEN_SPACE; ++tokenizer->at; } break;
        case '(': { token.type = TOKEN_LPAREN; ++tokenizer->at; } break;
        case ')': { token.type = TOKEN_RPAREN; ++tokenizer->at; } break;
        case '[': { token.type = TOKEN_LBRAC; ++tokenizer->at; } break;
        case ']': { token.type = TOKEN_RBRAC; ++tokenizer->at; } break;
        case '+': { token.type = TOKEN_PLUS; ++tokenizer->at; } break;
        case '*': { token.type = TOKEN_MUL; ++tokenizer->at; } break;
        case ':': { token.type = TOKEN_COLON; ++tokenizer->at; } break;
        case ',': { token.type = TOKEN_COMMA; ++tokenizer->at; } break;

        case '<':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '=')
            {
                token.type = TOKEN_LEQ;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_LT;
            }
        } break;
        case '/':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '=')
            {
                token.type = TOKEN_NEQ;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_UNKNOWN;
            }
        } break;
        case '=':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '=')
            {
                token.type = TOKEN_EQ;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_ASSIGN;
            }
        } break;
        case '&':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '&')
            {
                token.type = TOKEN_AND;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_UNKNOWN;
            }
        } break;
        case '|':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '|')
            {
                token.type = TOKEN_OR;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_UNKNOWN;
            }
        } break;
        case '-':
        {
            ++tokenizer->at;
            if (tokenizer->at[0] && tokenizer->at[0] == '>')
            {
                token.type = TOKEN_ARROW;
                token.textLength = 2;
                ++tokenizer->at;
            }
            else
            {
                token.type = TOKEN_MINUS;
            }
        } break;

        default:
        {
            if (isAlpha(tokenizer->at[0]))
            {
                while (isAlpha(tokenizer->at[0]) ||
                       isNumber(tokenizer->at[0]) ||
                       tokenizer->at[0] == '_')
                {
                    ++tokenizer->at;
                }

                token.type = TOKEN_ID;
                token.textLength = tokenizer->at - token.text;

                if (tokenEquals(token, "let")) { token.type = TOKEN_LET; }
                else if (tokenEquals(token, "in")) { token.type = TOKEN_IN; }
                else if (tokenEquals(token, "if")) { token.type = TOKEN_IF; }
                else if (tokenEquals(token, "then")) { token.type = TOKEN_THEN; }
                else if (tokenEquals(token, "else")) { token.type = TOKEN_ELSE; }
                else if (tokenEquals(token, "True")) { token.type = TOKEN_TRUE; }
                else if (tokenEquals(token, "False")) { token.type = TOKEN_FALSE; }
            }
            else if (isNumber(tokenizer->at[0]))
            {
                while (isNumber(tokenizer->at[0]))
                {
                    ++tokenizer->at;
                }

                token.type = TOKEN_NUM;
                token.textLength = tokenizer->at - token.text;
            }
            else
            {
                token.type = TOKEN_UNKNOWN;
                ++tokenizer->at;
            }
        } break;
    }

    /*printf("%d: %.*s\n", token.type, token.textLength, token.text);*/
    return token;
}

Token
getNonSpaceToken(Tokenizer *tokenizer)
{
    Token token;
    do
    {
        token = getToken(tokenizer);
    } while(token.type == TOKEN_SPACE);
    return token;
}
