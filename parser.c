#include <stdio.h>
#include <stdlib.h>
#include "lexer.h"
#include "parser.h"

#define DEBUG_PARSER 0

unsigned int
stringLength(char *string)
{
    unsigned int length = 0;
    while (*string != '\0')
    {
        ++length;
        ++string;
    }
    return length;
}

unsigned int
stringCopy(char *dest, char *src, int srcLength)
{
    int length = srcLength;
    while (length--)
    {
        *dest++ = *src++;
    }
    return srcLength;
}

void
astAppend(Tokenizer *tokenizer, char *asNode, unsigned int asNodeLength)
{
    unsigned int length = asNodeLength ? asNodeLength : stringLength(asNode);

    unsigned int newLength = tokenizer->astLength + length;
    if (newLength >= tokenizer->astCapacity)
    {
        tokenizer->astCapacity = newLength * 2;
        char *newAst = realloc(tokenizer->ast, tokenizer->astCapacity);
        if (newAst)
        {
            tokenizer->ast = newAst;
        }
        else
        {
            fprintf(stderr, "realloc failed!\n");
        }
    }

    length = stringCopy(tokenizer->ast + tokenizer->astLength, asNode, length);
    tokenizer->astLength += length;
}

void
astPrepend(Tokenizer *tokenizer, unsigned int prevLength, char *prepend)
{
    unsigned int diffLength = tokenizer->astLength - prevLength;
    char diff[diffLength];
    stringCopy(diff, tokenizer->ast + prevLength, diffLength);
    tokenizer->astLength = prevLength;

    astAppend(tokenizer, prepend, 0);
    astAppend(tokenizer, diff, diffLength);
    astAppend(tokenizer, ") (", 0);
}

void
requireTokenAndMaintainSpacing(Tokenizer *tokenizer, Token_Type desiredType)
{
    Token token;
    do
    {
        token = getToken(tokenizer);
        if (token.type == TOKEN_EOF)
        {
            break;
        }
        else if (token.type == TOKEN_SPACE)
        {
            astAppend(tokenizer, token.text, token.textLength);
        }
    } while (token.type != desiredType);
}

/*
 * Bxpr : TNUM
 *      | true
 *      | false
 *      | '(' Expr ')'
 *      | ID
 *      | '[' ']'
 *      | '[' Exprs ']'
 */
void
parseBxpr(Token token, Tokenizer *tokenizer)
{
    switch (token.type)
    {
        case TOKEN_NUM:
        {
            astAppend(tokenizer, "EInt ", 0);
            astAppend(tokenizer, token.text, token.textLength);
        } break;

        case TOKEN_FALSE:
        case TOKEN_TRUE:
        {
            astAppend(tokenizer, "EBool ", 0);
            astAppend(tokenizer, token.text, token.textLength);
        } break;

        case TOKEN_ID:
        {
            if (tokenEquals(token, "true") || tokenEquals(token, "false"))
            {
                astAppend(tokenizer, "EBool ", 0);
                astAppend(tokenizer, token.text, token.textLength);
            }
            else
            {
                astAppend(tokenizer, "EVar \\\"", 0);
                astAppend(tokenizer, token.text, token.textLength);
                astAppend(tokenizer, "\\\"", 0);
            }
        } break;

        case TOKEN_LPAREN:
        {
            astAppend(tokenizer, token.text, token.textLength);
            parseExpr(getToken(tokenizer), tokenizer);

            token = getNonSpaceToken(tokenizer);
            if (token.type == TOKEN_RPAREN)
            {
                astAppend(tokenizer, token.text, token.textLength);
            }
            else
            {
                fprintf(stderr, "parse error: need rparen\n");
            }
        } break;

        case TOKEN_LBRAC:
        {
            token = getNonSpaceToken(tokenizer);
            if (token.type == TOKEN_RBRAC)
            {
                astAppend(tokenizer, "ENil", 0);
            }
            else
            {
                astAppend(tokenizer, "EBin Cons (", 0);
                parseExpr(token, tokenizer);
                astAppend(tokenizer, ")", 0);

                int numberOfElementsExceptFirstElement = 0;
                for (;;)
                {
                    token = getNonSpaceToken(tokenizer);
                    if (token.type == TOKEN_RBRAC)
                    {
                        break;
                    }
                    else if (token.type == TOKEN_COMMA)
                    {
                        astAppend(tokenizer, " (EBin Cons (", 0);
                        ++numberOfElementsExceptFirstElement;
                    }
                    else
                    {
                        parseExpr(token, tokenizer);
                        astAppend(tokenizer, " )", 0);
                    }
                }

                astAppend(tokenizer, " (ENil)", 0);

                while (numberOfElementsExceptFirstElement--)
                {
                    astAppend(tokenizer, ")", 0);
                }
            }
        } break;

        default:
        {
        } break;
    }
}

void
parseAxprPrime(Token token, Tokenizer *tokenizer, unsigned int prevLength)
{
    switch (token.type)
    {
        case TOKEN_EOF: { } break;

        case TOKEN_NUM:
        case TOKEN_TRUE:
        case TOKEN_FALSE:
        case TOKEN_LPAREN:
        case TOKEN_LBRAC:
        case TOKEN_ID:
        {
            astPrepend(tokenizer, prevLength, "EApp (");
            parseBxpr(token, tokenizer);
            astAppend(tokenizer, ")", 0);
            parseAxprPrime(getNonSpaceToken(tokenizer), tokenizer, prevLength);
        } break;

        default:
        {
            tokenizer->at -= token.textLength;
        } break;
    }
}

// Axpr : BxprAxpr'
// Axpr' : Bxpr Bxpr
void
parseAxpr(Token token, Tokenizer *tokenizer)
{
    unsigned int prevLength = tokenizer->astLength;
    parseBxpr(token, tokenizer);

    token = getToken(tokenizer);
    if (token.type == TOKEN_SPACE)
    {
        parseAxprPrime(getToken(tokenizer), tokenizer, prevLength);
    }
    else
    {
        tokenizer->at -= token.textLength;
    }
}


void
parseBinOpExpr(Tokenizer *tokenizer, unsigned int prevLength, char *op)
{
    char buf[16];
    snprintf(buf, 16, "EBin %s (", op);
    astPrepend(tokenizer, prevLength, buf);
    parseExpr(getNonSpaceToken(tokenizer), tokenizer);
    astAppend(tokenizer, ")", 0);
    parseExprPrime(getNonSpaceToken(tokenizer), tokenizer, prevLength);
}

void
parseExprPrime(Token token, Tokenizer *tokenizer, unsigned int prevLength)
{
    switch (token.type)
    {
        case TOKEN_EOF: { } break;

        case TOKEN_COLON: { parseBinOpExpr(tokenizer, prevLength, "Cons"); } break;
        case TOKEN_AND: { parseBinOpExpr(tokenizer, prevLength, "And"); } break;
        case TOKEN_OR: { parseBinOpExpr(tokenizer, prevLength, "Or"); } break;
        case TOKEN_EQ: { parseBinOpExpr(tokenizer, prevLength, "Eq"); } break;
        case TOKEN_LT: { parseBinOpExpr(tokenizer, prevLength, "Lt"); } break;
        case TOKEN_LEQ: { parseBinOpExpr(tokenizer, prevLength, "Le"); } break;
        case TOKEN_PLUS: { parseBinOpExpr(tokenizer, prevLength, "Plus"); } break;
        case TOKEN_MINUS: { parseBinOpExpr(tokenizer, prevLength, "Minus"); } break;
        case TOKEN_MUL: { parseBinOpExpr(tokenizer, prevLength, "Mul"); } break;

        default:
        {
            tokenizer->at -= token.textLength;
        } break;
    }
}

// Expr : AxprExpr'
// Expr' : +ExprExpr'
void
parseExpr(Token token, Tokenizer *tokenizer)
{
    switch (token.type)
    {
        case TOKEN_EOF: { } break;

        case TOKEN_UNKNOWN:
        {
            parseExpr(getToken(tokenizer), tokenizer);
        } break;

        case TOKEN_SPACE:
        {
            astAppend(tokenizer, token.text, token.textLength);
            parseExpr(getToken(tokenizer), tokenizer);
        } break;

        case TOKEN_LAM:
        {
            astAppend(tokenizer, "ELam \\\"", 0);
            token = getNonSpaceToken(tokenizer);
            if (token.type == TOKEN_ID)
            {
                astAppend(tokenizer, token.text, token.textLength);
                if (requireToken(tokenizer, TOKEN_ARROW))
                {
                    astAppend(tokenizer, "\\\" (", 0);
                    parseExpr(getNonSpaceToken(tokenizer), tokenizer);
                    astAppend(tokenizer, ")", 0);
                }
                else
                {
                    fprintf(stderr, "parse error: expected ELam arrow\n");
                }
            }
        } break;

        case TOKEN_LET:
        {
            astAppend(tokenizer, "ELet ", 0);

            token = getNonSpaceToken(tokenizer);
            if (token.type == TOKEN_ID)
            {
                astAppend(tokenizer, "\\\"", 0);
                astAppend(tokenizer, token.text, token.textLength);
                astAppend(tokenizer, "\\\" (", 0); // ELet Expr 1

                token = getNonSpaceToken(tokenizer);
                if (token.type == TOKEN_ASSIGN)
                {
                    parseExpr(getToken(tokenizer), tokenizer);
                    astAppend(tokenizer, ")", 0); // ELet Expr 1

                    requireTokenAndMaintainSpacing(tokenizer, TOKEN_IN);

                    astAppend(tokenizer, " (", 0);
                    parseExpr(getNonSpaceToken(tokenizer), tokenizer);
                    astAppend(tokenizer, ")", 0);
                }
                else if (token.type == TOKEN_ID)
                {
                    int numArgs = 0;
                    do
                    {
                        ++numArgs;
                        astAppend(tokenizer, "ELam \\\"", 0);
                        astAppend(tokenizer, token.text, token.textLength);
                        astAppend(tokenizer, "\\\" (", 0);
                        token = getNonSpaceToken(tokenizer);
                    } while (token.type != TOKEN_ASSIGN);

                    parseExpr(getNonSpaceToken(tokenizer), tokenizer);

                    while (numArgs--)
                    {
                        astAppend(tokenizer, ")", 0);
                    }
                    astAppend(tokenizer, ")", 0); // ELet Expr 1

                    requireTokenAndMaintainSpacing(tokenizer, TOKEN_IN);

                    astAppend(tokenizer, " (", 0);
                    parseExpr(getNonSpaceToken(tokenizer), tokenizer);
                    astAppend(tokenizer, ")", 0);
                }
                else
                {
                    fprintf(stderr, "parse error: expected ELet =\n");
                }
            }
            else
            {
                fprintf(stderr, "parse error: expected ELet ID\n");
            }
        } break;

        case TOKEN_IF:
        {
            astAppend(tokenizer, "EIf ", 0);
            astAppend(tokenizer, "(", 0);
            parseExpr(getToken(tokenizer), tokenizer);
            astAppend(tokenizer, ") ", 0);

            requireTokenAndMaintainSpacing(tokenizer, TOKEN_THEN);

            astAppend(tokenizer, "(", 0);
            parseExpr(getToken(tokenizer), tokenizer);
            astAppend(tokenizer, ") ", 0);

            requireTokenAndMaintainSpacing(tokenizer, TOKEN_ELSE);

            astAppend(tokenizer, "(", 0);
            parseExpr(getToken(tokenizer), tokenizer);
            astAppend(tokenizer, ")", 0);
        } break;

        default:
        {
            unsigned int prevLength = tokenizer->astLength;
            parseAxpr(token, tokenizer);
            parseExprPrime(getNonSpaceToken(tokenizer), tokenizer, prevLength);
        } break;
    }
}

/*
 * Top : ID '=' Expr
 *     | Expr
 */
void
grammarParser(Tokenizer *tokenizer)
{
    Token token = getToken(tokenizer);
    /*printf("%d: %.*s\n", token.type, token.textLength, token.text);*/

    switch (token.type)
    {
        case TOKEN_EOF: { } break;
        case TOKEN_UNKNOWN: { grammarParser(tokenizer); } break;
        case TOKEN_SPACE:
        {
            astAppend(tokenizer, token.text, token.textLength);
            grammarParser(tokenizer);
        } break;

        case TOKEN_ID:
        {
            if (requireToken(tokenizer, TOKEN_ASSIGN))
            {
                astAppend(tokenizer, token.text, token.textLength);
                astAppend(tokenizer, "=", 0);
                parseExpr(getToken(tokenizer), tokenizer);
            }
            else
            {
                parseExpr(token, tokenizer);
            }
        } break;

        default:
        {
            parseExpr(token, tokenizer);
        } break;
    }
}

void
opPrecParseExpr(Tokenizer *tokenizer )
{
    Token token = getToken(tokenizer);
    switch (token.type)
    {
        case TOKEN_EOF: { } break;

        case TOKEN_LPAREN:
        {
            astAppend(tokenizer, "((((", 0);
            opPrecParseExpr(tokenizer);
        } break;

        case TOKEN_RPAREN:
        {
            astAppend(tokenizer, "))))", 0);
            opPrecParseExpr(tokenizer);
        } break;

        case TOKEN_PLUS:
        {
            astAppend(tokenizer, "))", 0);
            astAppend(tokenizer, token.text, token.textLength);
            astAppend(tokenizer, "((", 0);
            opPrecParseExpr(tokenizer);
        } break;

        case TOKEN_MUL:
        {
            astAppend(tokenizer, ")", 0);
            astAppend(tokenizer, token.text, token.textLength);
            astAppend(tokenizer, "(", 0);
            opPrecParseExpr(tokenizer);
        } break;

        default:
        {
            astAppend(tokenizer, token.text, token.textLength);
            opPrecParseExpr(tokenizer);
        } break;
    }
}

void
operatorPrecedenceParser(Tokenizer *tokenizer)
{
    astAppend(tokenizer, "((", 0);
    opPrecParseExpr(tokenizer);
    astAppend(tokenizer, "))", 0);
}

SizePair
parser(char *input, char **output)
{
    Tokenizer tokenizer;
    tokenizer.at = input;

    int capacity = 16;
    char *operatorAst = calloc(1, capacity);
    tokenizer.ast = operatorAst;
    tokenizer.astCapacity = capacity;
    tokenizer.astLength = 0;

    operatorPrecedenceParser(&tokenizer);
    operatorAst = tokenizer.ast;

    operatorAst[tokenizer.astLength] = '\0';
#if DEBUG_PARSER
    printf("DEBUG: capacity=%d length=%d operatorAst=%s\n",
            tokenizer.astCapacity, tokenizer.astLength, operatorAst);
#endif

    tokenizer.at = operatorAst;
    char *parseAst = calloc(1, capacity);
    tokenizer.ast = parseAst;
    tokenizer.astCapacity = capacity;
    tokenizer.astLength = 0;

    grammarParser(&tokenizer);
    parseAst = tokenizer.ast;

    parseAst[tokenizer.astLength] = '\0';
#if DEBUG_PARSER
    printf("DEBUG: capacity=%d length=%d parseAst=%s\n",
            tokenizer.astCapacity, tokenizer.astLength, parseAst);
#endif

    *output = parseAst;

    free(operatorAst);

    SizePair szPair;
    szPair.length = tokenizer.astLength;
    szPair.capacity = tokenizer.astCapacity;

    return szPair;
}
