# philang

A programming language that includes an 2-pass recursive descent parser (1 pass
for operator precendence and another for language grammar ast) and
contraint-based type inference interpreter.

To build run `make` and to test run `make tests`.
