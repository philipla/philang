#ifndef PARSER_H
#define PARSER_H

typedef struct
{
    unsigned int length;
    unsigned int capacity;
} SizePair;

unsigned int stringLength(char *string);
unsigned int stringCopy(char *dest, char *src, int srcLength);
void astAppend(Tokenizer *tokenizer, char *asNode, unsigned int asNodeLength);
void requireTokenAndMaintainSpacing(Tokenizer *tokenizer, Token_Type desiredType);

void parseBxpr(Token token, Tokenizer *tokenizer);
void parseAxpr(Token token, Tokenizer *tokenizer);
void parseExpr(Token token, Tokenizer *tokenizer);
void parseExprPrime(Token token, Tokenizer *tokenizer, unsigned int prevLength);

void operatorPrecedenceParser(Tokenizer *tokenizer);

SizePair parser(char *input, char **output);

#endif
