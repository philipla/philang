#include <stdio.h>
#include <stdlib.h>
#include "lexer.h"
#include "parser.h"

#define DEBUG_INTERPRET 0

void
interpret(char *input)
{
    char *output;

    SizePair szPair = parser(input, &output);

    char *command = "ghc Eval.hs Types.hs TypeCheck.hs -e \"evaluate $ ";
    unsigned int commandLength = stringLength(command);
    unsigned int newLength = szPair.length + commandLength;

    if (newLength >= szPair.capacity)
    {
        szPair.capacity = newLength * 2;
        output = realloc(output, szPair.capacity);
    }

    char *copyOfOutput = malloc(szPair.capacity);
    stringCopy(copyOfOutput, output, szPair.length);

    stringCopy(output, command, commandLength);
    stringCopy(output + commandLength, copyOfOutput, szPair.length);
    stringCopy(output + newLength, "\"", 1);

    output[newLength+1] = '\0';
#if DEBUG_INTERPRET
    printf("DEBUG: capacity=%d length=%d output=%s\n",
            szPair.capacity, newLength, output);
#endif

    system(output);

    free(copyOfOutput);
    free(output);
}

int
main(void)
{
    char inputBuffer[1024];

    for (;;)
    {
        printf("> ");
        fgets(inputBuffer, sizeof(inputBuffer), stdin);
        interpret(inputBuffer);
    }

    return 0;
}
