#ifndef LEXER_H
#define LEXER_H

typedef enum
{
    TOKEN_AND,
    TOKEN_OR,
    TOKEN_LT,
    TOKEN_EQ,
    TOKEN_LEQ,
    TOKEN_NEQ,
    TOKEN_LAM,
    TOKEN_NUM,
    TOKEN_ID,
    TOKEN_ARROW,
    TOKEN_ASSIGN,
    TOKEN_PLUS,
    TOKEN_MINUS,
    TOKEN_MUL,
    TOKEN_LPAREN,
    TOKEN_RPAREN,
    TOKEN_LBRAC,
    TOKEN_RBRAC,
    TOKEN_COLON,
    TOKEN_COMMA,
    TOKEN_SPACE,
    TOKEN_LET,
    TOKEN_IN,
    TOKEN_IF,
    TOKEN_THEN,
    TOKEN_ELSE,
    TOKEN_TRUE,
    TOKEN_FALSE,
    TOKEN_EOF,
    TOKEN_UNKNOWN,
} Token_Type;

typedef struct
{
    char *at;
    char *ast;
    unsigned int astLength;
    unsigned int astCapacity;
} Tokenizer;

typedef struct
{
    Token_Type type;
    int textLength;
    char *text;
} Token;


char isAlpha(char c);
char isNumber(char c);
char isEndOfLine(char c);
char isTabOrLine(char c);

void removeTabsLinesComments(Tokenizer *tokenizer);

char tokenEquals(Token token, char *match);
char requireToken(Tokenizer *tokenizer, Token_Type desiredType);

Token getToken(Tokenizer *tokenizer);
Token getNonSpaceToken(Tokenizer *tokenizer);

#endif
